const Group = require('../models/group');

//Create a group
exports.create = function (req, res, next) {
    let group = new Group(
        {
            ...req.body
        }
    );

    group.save(function (err) {
        if (err) return next(err);
        res.send(group.toJSON());
    })
}

//Find group by id
exports.findById = function (req, res, next) {
    Group.findById(req.params.id)
        .populate({
            path: 'roles'
        })
        .exec(function (err, group) {
            if (err) {
                return next(err);
            }
            res.send(group);
        });
}

//Find all groups
exports.findAll = function (req, res, next) {
    Group.find({}, function (err, groups) {
        if (err) return next(err);
        res.send(groups);
    }).populate({
        path: 'roles'
    })
}

//Update a groups
exports.update = function (req, res, next) {
    // Avoid updates with roles
    let removePotentialRoles = req.body;
    Group.findByIdAndUpdate(req.params.id, { $set: removePotentialRoles }, function (err, group) {
        if (err) return next(err);
        res.send(group);
    });
}

//Delete group by id
exports.delete = function (req, res, next) {
    Group.findByIdAndRemove(req.params.id, function (err, group) {
        if (err) return next(err);
        res.send(group);
    });
}

//Link a group to any role
exports.associateRole = function (req, res, next) {
    Group.findByIdAndUpdate({ _id: req.params.id }, { $addToSet: { roles: req.params.gid } }, { new: true }, function (err, group) {
        if (err) return next(err);
        res.send(group);
    });
}

//Unlink a group to any role
exports.disassociateRole = function (req, res, next) {
    User.findByIdAndUpdate({ _id: req.params.id }, { $pull: { roles: req.params.gid } }, { new: true }, function (err, group) {
        if (err) return next(err);
        res.send(group);
    });
}