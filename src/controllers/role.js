const Role = require('../models/role');

//Create a new role
exports.create = function(req, res, next) {
    let role = new Role(
        {
            ...req.body
        }
    );

    role.save(function(err) {
        if(err) return next(err);
        res.send(role.toJSON());
    })
}

//Find a role by id
exports.findById = function(req, res, next) {
    Role.findById(req.params.id, function(err, role) {
        if(err) return next(err);
        res.send(role);
    })
}

//Find all roles
exports.findAll = function(req, res, next){
    Role.find({}, function(err, roles) {
        if(err) return next(err);
        res.send(roles);
    })
}

//Update a role
exports.update = function(req, res, next) {
    Role.findByIdAndUpdate(req.params.id, { $set: req.body }, function(err, role) {
        if(err) return next(err);
        res.send(role);
    });
}

//Delete a role by id
exports.delete = function(req, res, next) {
    Role.findByIdAndRemove(req.params.id, function(err, role) {
        if(err) return next(err);
        res.send(role);
    });
}