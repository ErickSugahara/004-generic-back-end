const User = require('../models/user');

//Create a new user
exports.create = async function (req, res, next) {

  let user = new User(
    {
      ...req.body
    }
  );

  try {

    await user.save(function (err) {
      if (err) return next(err);

      // Remove user password hash from here
      // delete user.password;
      res.send(user);
    })
  } catch {
    return res.status(400).send({ error: 'Registration failed' });
  }
}

//Find user by id
exports.findById = async function (req, res, next) {
  await User.findById(req.params.id)
    .populate({
      path: 'groups',
      populate: {
        path: 'roles'
      }
    })
    .exec(function (err, user) {
      if (err) {
        return next(err);
      }
      res.send(user);
    });
}

//Find all users
exports.findAll = async function (req, res, next) {
  await User.find({}, function (err, users) {
    if (err) return next(err);
    res.send(users);
  }).populate({
    path: 'groups'
  })
}

//Update a user
exports.update = async function (req, res, next) {
  // Avoid updates with groups
  let removePotentialGroups = req.body;
  //delete removePotentialGroups.groups;
  await User.findByIdAndUpdate(req.params.id, { $set: removePotentialGroups }, function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
}

//Delete user by id
exports.delete = async function (req, res, next) {
  await User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
}

//Link a user to any group
exports.associateGroup = async function (req, res, next) {
  await User.findByIdAndUpdate({ _id: req.params.id }, { $addToSet: { groups: req.params.gid } }, { new: true }, function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
}

//Unlink a user to any group
exports.disassociateGroup = async function (req, res, next) {
  await User.findByIdAndUpdate({ _id: req.params.id }, { $pull: { groups: req.params.gid } }, { new: true }, function (err, user) {
    if (err) return next(err);
    res.send(user);
  });
}
