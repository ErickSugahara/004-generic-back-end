const mongoose = require('../database');

let groupSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    roles: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Role'
    }],
    active: { type: Boolean, required: true, default: true }
});

//Export group model
module.exports = mongoose.model('Group', groupSchema);