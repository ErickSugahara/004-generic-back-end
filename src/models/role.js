const mongoose = require('../database');

let roleSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    active: {type: Boolean, required: true, default: true}
});

//Export role model
module.exports = mongoose.model('Role', roleSchema);