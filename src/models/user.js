const mongoose = require('../database');
const bcrypt = require('bcryptjs');

let userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, max: 100, unique: true },
  cpf: { type: String, required: true },
  rg: { type: String, required: true },
  password: { type: String, required: true, select: false },
  //tel: { type: String, required: true },
  groups: [{
    type: mongoose.Schema.Types.ObjectId, ref: 'Group'
  }],
  active: { type: Boolean, default: true }
});

userSchema.pre('save', async function (next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;

  next();
})

//Export user model
module.exports = mongoose.model('User', userSchema);