//Definindo servidor express https://expressjs.com/pt-br/
const express = require('express');

//Proteger requisições
const helmet = require('helmet');
//Melhorar log de status do servidor
const morgan = require('morgan');

//Instanciando aplicação
const app = express();
//Iniciando helmet https://github.com/helmetjs/helmet
app.use(helmet());
//Configuração e iniciando do morgan https://github.com/expressjs/morgan
app.use(morgan('dev'));

//Setando porta por variável de ambiente. Default = 5000
const port = process.env.PORT || 5000;

//Conversor de requisição
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Allow-Credentials', "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Rotas
require('./routes/user')(app);
require('./routes/group')(app);
require('./routes/role')(app);
require('./routes/download')(app);

//Mensagem de servidor subiu
app.listen(port, () => console.log(`Listening on port ${port}`));