const express = require('express');
const router = express.Router();

const userController = require('../controllers/user');

router.get('/', userController.findAll);
router.get('/:id', userController.findById);
router.post('/', userController.create);
router.put('/:id', userController.update);
router.delete('/:id', userController.delete);

router.post('/:id/group/:gid', userController.associateGroup);
router.delete('/:id/group/:gid', userController.disassociateGroup);

module.exports = app => app.use('/user', router);