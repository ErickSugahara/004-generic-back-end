const express = require('express');
const router = express.Router();

const roleController = require('../controllers/role');

router.get('/', roleController.findAll);
router.get('/:id', roleController.findById);
router.post('/', roleController.create);
router.put('/:id', roleController.update);
router.delete('/:id', roleController.delete);

module.exports = app => app.use('/role', router);