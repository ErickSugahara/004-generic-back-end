const express = require("express");
const router = express.Router();

const groupController = require("../controllers/group");

router.get("/", groupController.findAll);
router.get("/:id", groupController.findById);
router.post("/", groupController.create);
router.put("/:id", groupController.update);
router.delete("/:id", groupController.delete);

module.exports = app => app.use("/group", router);
